package cl.ubb.TallerMecanico.controller.RESTController;

import cl.ubb.TallerMecanico.Exceptions.HerramientaNoEncontradaException;
import cl.ubb.TallerMecanico.domain.Herramienta;
import cl.ubb.TallerMecanico.service.implementation.HerramientaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/herramienta")
public class HerramientaController {

    @Autowired
    HerramientaService herramientaService;

    @PostMapping("/crear")
    public ResponseEntity<Herramienta> crearHerramienta(@RequestBody Herramienta herramienta) {
        herramientaService.crear(herramienta); //Recibe un objeto tipo herramienta y se lo entrega a herramientaService para que se inserte en la base de datos
        return new ResponseEntity<>(herramienta, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<Herramienta> actualizarHerramienta(@RequestBody Herramienta herramienta) throws HerramientaNoEncontradaException {
        herramientaService.actualizar(herramienta);
        return new ResponseEntity<>(herramienta, HttpStatus.OK);
    }

    @PutMapping("/borrar/{id}")
    public ResponseEntity<Herramienta> borrarHerramienta(@PathVariable("id") int id) throws HerramientaNoEncontradaException {
        System.out.println(id + "iddd");
        Herramienta herramientaAActualizar = herramientaService.buscarPorId(id);
        herramientaAActualizar.setVisible(false);
        Herramienta herramientaActualizada = herramientaService.actualizar(herramientaAActualizar);
        return new ResponseEntity<Herramienta>(herramientaActualizada, HttpStatus.OK);
    }
}
