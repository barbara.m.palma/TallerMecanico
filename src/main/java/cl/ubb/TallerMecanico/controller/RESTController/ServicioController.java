package cl.ubb.TallerMecanico.controller.RESTController;

import cl.ubb.TallerMecanico.Exceptions.ServicioNoEncontradoException;
import cl.ubb.TallerMecanico.domain.Servicio;
import cl.ubb.TallerMecanico.service.implementation.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/servicio")
public class ServicioController {

    @Autowired
    private ServicioService servicioService;

    @PostMapping("/crear")
    public ResponseEntity<Servicio> crearServicio(@RequestBody Servicio servicio) {
        servicioService.Ingresar(servicio); //Recibe un objeto tipo servicio y se lo entrega a servicioService para que se inserte en la base de datos
        return new ResponseEntity<>(servicio, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<Servicio> actualizarSevicio(@RequestBody Servicio servicio) throws ServicioNoEncontradoException {
        servicioService.actualizar(servicio);
        return new ResponseEntity<>(servicio, HttpStatus.OK);
    }

    @PutMapping("/borrar/{id}")
    public ResponseEntity<Servicio> borrarServicio(@PathVariable("id") int id) throws ServicioNoEncontradoException {
        Servicio ServicioActualizar = servicioService.buscarPorId(id);
        ServicioActualizar.setVisible(false);
        Servicio servicioACtualizado = servicioService.actualizar(ServicioActualizar);
        return new ResponseEntity<Servicio>(servicioACtualizado, HttpStatus.OK);
    }
}
