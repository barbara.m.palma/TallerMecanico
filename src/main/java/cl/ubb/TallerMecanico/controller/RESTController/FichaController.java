package cl.ubb.TallerMecanico.controller.RESTController;

import cl.ubb.TallerMecanico.domain.Ficha;
import cl.ubb.TallerMecanico.service.implementation.FichaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/ficha")
public class FichaController {

    @Autowired
    FichaService fichaService;

    @PostMapping("/crear")
    public ResponseEntity<Ficha> crearCiente(@RequestBody Ficha ficha) {
        System.out.println("llego aquí "+ficha.getIdAdministrativo()+ficha.getIdCliente()+ficha.getIdServicio());
        fichaService.crear(ficha); //Recibe un objeto tipo ficha y se lo entrega a fichaService para que se inserte en la base de datos
        return new ResponseEntity<>(ficha, HttpStatus.OK);
    }

}
