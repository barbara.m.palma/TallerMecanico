package cl.ubb.TallerMecanico.controller.RESTController;

import cl.ubb.TallerMecanico.Exceptions.MaquinariaNoEncontradaException;
import cl.ubb.TallerMecanico.domain.Maquinaria;
import cl.ubb.TallerMecanico.service.implementation.MaquinariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


//Procesa las acciones REST relacionadas con maquinaria
@Controller
@RequestMapping("/maquinaria")
public class MaquinariaController {

    @Autowired
    MaquinariaService maquinariaService;

    //Para la URL "/maquinaria/crear" se usa el verbo POST
    @PostMapping("/crear")
    public ResponseEntity<Maquinaria> crearMaquinaria(@RequestBody Maquinaria maquinaria) {
        maquinariaService.crear(maquinaria); //Recibe un objeto tipo maquinaria y se lo entrega a maquinariaService para que se inserte en la base de datos
        return new ResponseEntity<>(maquinaria, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<Maquinaria> actualizarMaquinaria(@RequestBody Maquinaria maquinaria) throws MaquinariaNoEncontradaException {
        maquinariaService.actualizar(maquinaria);
        return new ResponseEntity<>(maquinaria, HttpStatus.OK);
    }

    @PutMapping("/borrar/{id}")
    public ResponseEntity<Maquinaria> borrarMaquinaria(@PathVariable("id") int id) throws MaquinariaNoEncontradaException {
        Maquinaria MaquinariaActualizar = maquinariaService.buscarPorId(id);
        MaquinariaActualizar.setVisible(false);
        Maquinaria maquinariaActualizada = maquinariaService.actualizar(MaquinariaActualizar);
        return new ResponseEntity<Maquinaria>(maquinariaActualizada, HttpStatus.OK);
    }
}
