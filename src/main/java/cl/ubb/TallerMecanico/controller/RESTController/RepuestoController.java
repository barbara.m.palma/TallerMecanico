package cl.ubb.TallerMecanico.controller.RESTController;

import cl.ubb.TallerMecanico.Exceptions.RepuestoNoEncontradoException;
import cl.ubb.TallerMecanico.domain.Repuesto;
import cl.ubb.TallerMecanico.service.implementation.RepuestoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


//Procesa las acciones REST relacionadas con repuesto
@Controller
@RequestMapping("/repuesto")
public class RepuestoController {

    @Autowired
    RepuestoService repuestoService;

    //Para la URL "/maquinaria/crear" se usa el verbo POST
    @PostMapping("/crear")
    public ResponseEntity<Repuesto> crearRepuesto(@RequestBody Repuesto repuesto) {
        repuestoService.crear(repuesto); //Recibe un objeto tipo repuesto y se lo entrega a repuestoService para que se inserte en la base de datos
        return new ResponseEntity<>(repuesto, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<Repuesto> actualizarRepuesto(@RequestBody Repuesto repuesto) throws RepuestoNoEncontradoException {
        repuestoService.actualizar(repuesto);
        return new ResponseEntity<>(repuesto, HttpStatus.OK);
    }

    @PutMapping("/borrar/{id}")
    public ResponseEntity<Repuesto> borrarRepuesto(@PathVariable("id") int id) throws RepuestoNoEncontradoException {
        Repuesto RepuestoActualizar = repuestoService.buscarPorId(id);
        RepuestoActualizar.setVisible(false);
        Repuesto repuestoActualizado = repuestoService.actualizar(RepuestoActualizar);
        return new ResponseEntity<Repuesto>(repuestoActualizado, HttpStatus.OK);
    }
}
