package cl.ubb.TallerMecanico.controller.RESTController;


import cl.ubb.TallerMecanico.Exceptions.VehiculoNoEncontradoException;
import cl.ubb.TallerMecanico.domain.Vehiculo;
import cl.ubb.TallerMecanico.service.implementation.VehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/vehiculo")
public class VehiculoController {

    @Autowired
    VehiculoService vehiculoService;

    @PostMapping("/crear")
    public ResponseEntity<Vehiculo> crearVehiculo(@RequestBody Vehiculo vehiculo) {
        vehiculoService.crear(vehiculo); //Recibe un objeto tipo herramienta y se lo entrega a herramientaService para que se inserte en la base de datos
        return new ResponseEntity<>(vehiculo, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<Vehiculo> actualizarVehiculo(@RequestBody Vehiculo vehiculo) throws VehiculoNoEncontradoException {
        vehiculoService.actualizar(vehiculo);
        return new ResponseEntity<>(vehiculo, HttpStatus.OK);
    }

    @PutMapping("/borrar/{id}")
    public ResponseEntity<Vehiculo> borrarVehiculo(@PathVariable("id") int id) throws VehiculoNoEncontradoException {
        Vehiculo vehiculoAActualizar = vehiculoService.buscarPorId(id);
        vehiculoAActualizar.setVisible(false);
        Vehiculo vehiculoActualizado = vehiculoService.actualizar(vehiculoAActualizar);
        return new ResponseEntity<Vehiculo>(vehiculoActualizado, HttpStatus.OK);
    }

}
