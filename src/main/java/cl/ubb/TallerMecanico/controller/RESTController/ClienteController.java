package cl.ubb.TallerMecanico.controller.RESTController;

import cl.ubb.TallerMecanico.Exceptions.ClienteNoEncontradoException;
import cl.ubb.TallerMecanico.domain.Cliente;
import cl.ubb.TallerMecanico.service.implementation.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


//Procesa las acciones REST relacionadas con cliente
@Controller
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    //Para la URL "/cliente/crear" se usa el verbo POST
    @PostMapping("/crear")
    public ResponseEntity<Cliente> crearCiente(@RequestBody Cliente cliente) {
        clienteService.crear(cliente); //Recibe un objeto tipo cliente y se lo entrega a clienteService para que se inserte en la base de datos
        return new ResponseEntity<>(cliente, HttpStatus.OK);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<Cliente> actualizarCliente(@RequestBody Cliente cliente) throws ClienteNoEncontradoException {
        clienteService.actualizar(cliente);
        return new ResponseEntity<>(cliente, HttpStatus.OK);
    }

    @PutMapping("/borrar/{id}")
    public ResponseEntity<Cliente> borrarCliente(@PathVariable("id") int id) throws ClienteNoEncontradoException {
        Cliente clienteAActualizar = clienteService.buscarPorId(id);
        clienteAActualizar.setVisible(false);
        Cliente clienteActualizado = clienteService.actualizar(clienteAActualizar);
        return new ResponseEntity<Cliente>(clienteActualizado, HttpStatus.OK);
    }
}
