package cl.ubb.TallerMecanico.controller;

import cl.ubb.TallerMecanico.Exceptions.*;
import cl.ubb.TallerMecanico.domain.*;
import cl.ubb.TallerMecanico.reports.AdminExcelBuilder;
import cl.ubb.TallerMecanico.reports.AdminPdfBuilder;
import cl.ubb.TallerMecanico.reports.ServiceRecordExcelBuilder;
import cl.ubb.TallerMecanico.reports.ServiceRecordPdfBuilder;
import cl.ubb.TallerMecanico.service.*;
import cl.ubb.TallerMecanico.service.implementation.MaquinariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//WebController se encarga de mapear las url de la página web
@Controller
public class WebController {

    @Autowired
    IClienteService clienteService;

    @Autowired
    IHerramientaService herramientaService;

    @Autowired
    IMaquinariaService maquinariaService;

    @Autowired
    IRepuestoService repuestoService;

    @Autowired
    IServicioService servicioService;

    @Autowired
    IFichaService fichaService;

    @Autowired
    IVehiculoService vehiculoService;

    @Autowired
    IAdministrativoService administrativoService;

    @Autowired
    IFichaRutService fichaRutService;

    @Autowired
    ISPVService spvService;

    @GetMapping("/") //Para el URL "/"
    public ModelAndView index(Authentication authentication) {
        String role = authentication.getAuthorities().toString();
        String viewName = "index";

        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Home"); //Se entregan atributos como titulo, página activa e ícono a usar
        page.put("active", "home");
        page.put("glyph", "glyphicon glyphicon-home");

        if (role.equals("[ROLE_MECH]")) {
            viewName = "index-mecanico";
        } else if (role.equals("[ROLE_EJEC]")) {
            viewName = "index-ejecutivo";
        }
        return new ModelAndView(viewName).addObject("page", page);
        //Se retorna un objeto ModelAndView, al que se le indica cuál es el nombre del archivo HTML que se mostrará en esta URL y además se le entrega
    }

    /*-----------------------------------------------------------------------------------------------------*/

    //Clientes
    @GetMapping("/clientes")
    public ModelAndView clientes() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Editar Cliente");
        page.put("active", "Clientes");
        page.put("glyph", "glyphicon glyphicon-home");
        List<Cliente> clientes = clienteService.buscarVisibles(); //Se hace uso de clienteService para consultar todos los clientes en la base de datos
        return new ModelAndView("clientes").addObject("clientesData", clientes).addObject("page", page);
        //Al objeto ModelAndView se le agrega además la lista de Clientes consultada a clienteService
    }

    @GetMapping("/cliente/crear")
    public ModelAndView creaCliente() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Crear Cliente");
        return new ModelAndView("ingresar-cliente").addObject("page", page);
    }

    @GetMapping("/cliente/editar/{id}")
    public ModelAndView editarCliente(@PathVariable("id") int id) throws ClienteNoEncontradoException {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Editar cliente");
        page.put("glyph", "glyphicon glyphicon-user");

        Cliente cliente = clienteService.buscarPorId(id);
        return new ModelAndView("editar-cliente").addObject("page", page).addObject("cliente", cliente);
    }

    /*-------------------------------------------------------------------------------------------------*/

    //Herramientas
    @GetMapping("/herramientas")
    public ModelAndView herramientas() {
        HashMap<String, String> page = new HashMap<String, String>();
        List<Herramienta2> herramientas = herramientaService.buscarTodos();
        return new ModelAndView("herramientas").addObject("herramientasData",herramientas);
    }

    @GetMapping("/herramienta/crear")
    public ModelAndView creaHerramienta() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Crear Herramienta");
        return new ModelAndView("ingresar-herramienta").addObject("page", page);
    }

    @GetMapping("/herramienta/editar/{id}")
    public ModelAndView editarHerramienta(@PathVariable("id") int id) throws HerramientaNoEncontradaException {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Editar herramienta");
        page.put("glyph", "glyphicon glyphicon-user");
        Herramienta herramienta = herramientaService.buscarPorId(id);
        return new ModelAndView("editar-herramienta").addObject("page", page).addObject("herramienta", herramienta);
    }


    /*-----------------------------------------------------------------------------------------*/

    //Maquinaria
    @GetMapping("/maquinarias")
    public ModelAndView maquinarias() {
        HashMap<String, String> page = new HashMap<String, String>();
        List<Maquinaria2> maquinarias = maquinariaService.buscarTodos();
        return new ModelAndView("maquinarias").addObject("maquinariasData",maquinarias);
    }

    @GetMapping("/maquinaria/crear")
    public ModelAndView crearMaquinaria() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Crear maquinaria");
        return new ModelAndView("ingresar-maquinaria").addObject("page", page);
    }

    @GetMapping("/maquinaria/editar/{id}")
    public ModelAndView editarMaquinaria(@PathVariable("id") int id) throws MaquinariaNoEncontradaException {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Editar maquinaria");
        page.put("glyph", "glyphicon glyphicon-user");
        Maquinaria maquinaria = maquinariaService.buscarPorId(id);
        return new ModelAndView("editar-maquinaria").addObject("page", page).addObject("maquinaria", maquinaria);
    }

    /*---------------------------------------------------------------------------------*/

    //Repuesto
    @GetMapping("/repuestos")
    public ModelAndView repuestos() {
        HashMap<String, String> page = new HashMap<String, String>();
        List<Repuesto2> repuestos = repuestoService.buscarTodos();
        return new ModelAndView("repuestos").addObject("repuestosData", repuestos);
    }

    @GetMapping("/repuesto/crear")
    public ModelAndView crearRepuesto() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Crear repuesto");
        return new ModelAndView("ingresar-repuesto").addObject("page", page);
    }

    @GetMapping("/repuesto/editar/{id}")
    public ModelAndView editarRepuesto(@PathVariable("id") int id) throws RepuestoNoEncontradoException {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Editar repuesto");
        page.put("glyph", "glyphicon glyphicon-user");
        Repuesto repuesto = repuestoService.buscarPorId(id);
        return new ModelAndView("editar-repuesto").addObject("page", page).addObject("repuesto", repuesto);
    }

    /*---------------------------------------------------------------------------------*/

    //Servicios
    @GetMapping("/servicios")
    public ModelAndView servicios() {
        HashMap<String, String> page = new HashMap<String, String>();
        List<Servicio> servicios = servicioService.buscarVisibles();
        return new ModelAndView("servicios").addObject("serviciosData",servicios);
    }

    @GetMapping("/servicios/ingresar")
    public ModelAndView ingresarServicios() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Ingresar servicios");
        return new ModelAndView("ingresar-servicios").addObject("page", page);
    }

    @GetMapping("/servicio/editar/{id}")
    public ModelAndView editarServicio(@PathVariable("id") int id) throws ServicioNoEncontradoException {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Editar servicio");
        page.put("glyph", "glyphicon glyphicon-user");
        Servicio servicio = servicioService.buscarPorId(id);
        return new ModelAndView("editar-servicio").addObject("page", page).addObject("servicio", servicio);
    }

    /*-------------------------------------------------------------*/

    //cuando se le asigna un servicio a un vehiculo
    @GetMapping("/ficha/vehiculo/{id}")
    public ModelAndView verFichaVehiculo(@PathVariable("id") int id) {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Ficha vehículo");
        List<FichaServicio> ficha = fichaService.buscarPorVehiculo(id);
        Vehiculo vehiculo = vehiculoService.buscarPorId(id);
        return new ModelAndView("ficha-vehiculo")
                .addObject("page", page)
                .addObject("ficha", ficha)
                .addObject("vehiculo", vehiculo);

    }
    /*---------------------------------------------------------------------------------*/

    @GetMapping("/mecanico/crear/servicio")
    public ModelAndView crearFicha() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Servicio mecanico");
        List<Servicio> servicios = servicioService.buscarVisibles();
        List<Administrativo> administrativos = administrativoService.buscarTodos();
        List<Cliente> clientes = clienteService.buscarVisibles();
        List<Vehiculo> vehiculos = vehiculoService.buscarVisibles();
        return new ModelAndView("ingresar-servicio-mecanico")
                .addObject("page", page)
                .addObject("servicios", servicios)
                .addObject("admins", administrativos)
                .addObject("clientes", clientes)
                .addObject("vehiculos", vehiculos);

    }

    @GetMapping("/ficha/crear/{id}")
    public ModelAndView crearFicha(@PathVariable("id") int id) {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Agregar ficha");
        List<Servicio> servicios = servicioService.buscarVisibles();
        List<Administrativo> administrativos = administrativoService.buscarTodos();
        List<Cliente> clientes = clienteService.buscarVisibles();
        Vehiculo vehiculo = vehiculoService.buscarPorId(id);
        return new ModelAndView("ingresar-ficha")
                .addObject("page", page)
                .addObject("servicios", servicios)
                .addObject("admins", administrativos)
                .addObject("clientes", clientes)
                .addObject("vehiculo", vehiculo);

    }

    @GetMapping("/fichas")
    public ModelAndView fichas() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Ficha");
        List<FichaRUT> fichas = fichaRutService.buscarTodas();
        return new ModelAndView("fichas")
                .addObject("page", page)
                .addObject("fichas", fichas);

    }

    @GetMapping("/ficha/{id}")
    public ModelAndView verFicha(@PathVariable("id") int id) {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Ficha detalle");
        Ficha ficha = fichaService.buscarPorId(id);
        Vehiculo vehiculo = vehiculoService.buscarPorId(ficha.getIdVehiculo());
        Cliente cliente = clienteService.buscarPorId(ficha.getIdCliente());
        Administrativo administrativo = administrativoService.buscarPorId(ficha.getIdAdministrativo());
        Servicio servicio = servicioService.buscarPorId(ficha.getIdServicio());
        return new ModelAndView("ficha-detalle")
                .addObject("page", page)
                .addObject("ficha", ficha)
                .addObject("cliente", cliente)
                .addObject("vehiculo", vehiculo)
                .addObject("administrativo", administrativo)
                .addObject("servicio", servicio);

    }

    /*-------------------------------------------------------------*/

    //Vehiculo
    @GetMapping("/vehiculos")
    public ModelAndView vehiculos() {
        HashMap<String, String> page = new HashMap<String, String>();
        page.put("title", "Vehículos");
        List<Vehiculo> vehiculos = vehiculoService.buscarVisibles();
        return new ModelAndView("vehiculos")
                .addObject("page", page)
                .addObject("vehiculos", vehiculos);

    }

    /*---------------------------------------------------------------------------------*/

    //Reportes
    @GetMapping("/downloadAdminExcel")
    public ModelAndView downloadAdminExcel() {
        Map<String, Object> model = new HashMap<String, Object>();

        //Falta obtener las consultas
        List<Herramienta> herramientas = herramientaService.buscarVisibles();
        List<Maquinaria> maquinarias = maquinariaService.buscarVisibles();
        List<Repuesto> repuestos =  repuestoService.buscarVisibles();

        model.put("herramientas", herramientas);
        model.put("maquinarias", maquinarias);
        model.put("repuestos", repuestos);

        return new ModelAndView(new AdminExcelBuilder(), "model", model);
    }

    @GetMapping("/downloadAdminPdf")
    public ModelAndView downloadAdminPdf() {
        Map<String, Object> model = new HashMap<String, Object>();

        //Falta obtener las consultas
        List<Herramienta> herramientas = herramientaService.buscarVisibles();
        List<Maquinaria> maquinarias = maquinariaService.buscarVisibles();
        List<Repuesto> repuestos =  repuestoService.buscarVisibles();

        model.put("herramientas", herramientas);
        model.put("maquinarias", maquinarias);
        model.put("repuestos", repuestos);

        return new ModelAndView(new AdminPdfBuilder(), "model", model);
    }

    @GetMapping("/downloadSPVExcel")
    public ModelAndView downloadSPVExcel() {
        Map<String, Object> model = new HashMap<String, Object>();
        List<SPV> spv = spvService.buscarTodos();
        model.put("spv", spv);
        return new ModelAndView(new ServiceRecordExcelBuilder(), "model", model);
    }

    @GetMapping("/downloadSPVPdf")
    public ModelAndView downloadSPVPdf() {
        Map<String, Object> model = new HashMap<String, Object>();
        List<SPV> spv = spvService.buscarTodos();
        model.put("spv", spv);
        return new ModelAndView(new ServiceRecordPdfBuilder(), "model", model);
    }

    //Error
    @GetMapping("/error")
    public ModelAndView error() {
        return new ModelAndView("error");
    }



}
