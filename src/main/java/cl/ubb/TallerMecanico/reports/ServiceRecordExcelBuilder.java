package cl.ubb.TallerMecanico.reports;

import cl.ubb.TallerMecanico.domain.SPV;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.*;

import org.springframework.web.servlet.view.document.AbstractXlsxView;
import org.apache.poi.ss.usermodel.*;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTCol;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ServiceRecordExcelBuilder extends AbstractXlsxView{
    final static String path = System.getProperty("user.dir") + "\\src\\main\\resources\\excelTemplates\\";
    final static String logo = "titleLogo.png";
    final static DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
    final static String fileName = "Reporte_SPV_" + dateFormat.format(new Date()).toString() + ".xlsx";

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Se obtiene el modelo que contiene las listas
        Map<String, Object> listsModel = (Map<String, Object>) model.get("model");

        //Se obtienen las listas contenidas en el modelo
        List<SPV> spv = (List<SPV>) listsModel.get("spv");

        //Se crea una nuva hoja de calculo
        XSSFSheet sheet;
        Row row;
        Cell cell;
        Font font;
        CellStyle style;

        //Declaración de variables
        int rowNum = 2;
        int firstRow = 2;
        int lastRow = 1000;
        int firstCol = 0;
        int lastCol = 6;
        int lastColInDoc = 16384;
        int cellWidth = 256 * 28;
        String[] headers = {"Servicio", "Fecha", "Marca", "Modelo", "Año", "Patente"};

        //Se crea una nueva hoja de cálculo
        sheet = (XSSFSheet) workbook.createSheet();
        sheet.setZoom(100);
        row = sheet.createRow(0);
        row.setHeight((short) (256*4));
        sheet.addMergedRegion(new CellRangeAddress(0, 0, firstCol, lastCol));

        //Se añade la imagen
        FileInputStream inputImage = new FileInputStream(new File(path + logo));
        int pictureIndex = workbook.addPicture(IOUtils.toByteArray(inputImage), Workbook.PICTURE_TYPE_PNG);
        Drawing drawing = sheet.createDrawingPatriarch();
        ClientAnchor anchor = workbook.getCreationHelper().createClientAnchor();
        anchor.setAnchorType(ClientAnchor.AnchorType.DONT_MOVE_AND_RESIZE);
        anchor.setRow1(0);
        anchor.setRow2(0);
        anchor.setCol1(0);
        anchor.setCol2(0);
        Picture picture = drawing.createPicture(anchor, pictureIndex);
        picture.resize();

        //Se rellenan y configuran las cabeceras
        font = sheet.getWorkbook().createFont();
        font.setFontName("Calibri");
        font.setBold(true);
        style = sheet.getWorkbook().createCellStyle();
        style.setFont(font);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LIGHT_CORNFLOWER_BLUE.getIndex());
        style.setAlignment(HorizontalAlignment.CENTER);

        row = sheet.createRow(1);
        for (int cellNum = 0; cellNum < headers.length; cellNum++) {
            cell = row.createCell(cellNum);
            cell.setCellValue(headers[cellNum]);
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(style);

            if (cellNum == 0) {
                sheet.setColumnWidth(cellNum, cellWidth+(256*18));
            } else {
                sheet.setColumnWidth(cellNum, cellWidth);

            }
        }

        //Se ocultan las celdas que no se utilizarán
        CTCol col = sheet.getCTWorksheet().getColsArray(0).addNewCol();
        col.setMin(lastCol+1);
        col.setMax(lastColInDoc);
        col.setHidden(true);

        //Se se configuran las fuentes y e l estilo para las celdas
        font = sheet.getWorkbook().createFont();
        font.setFontName("Calibri");
        style = sheet.getWorkbook().createCellStyle();
        style.setFont(font);
        style.setAlignment(HorizontalAlignment.CENTER);

        //Se configura la celda de fecha
        CellStyle styleDate = workbook.createCellStyle();
        styleDate.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat("dd/mm/yyyy"));
        styleDate.setFont(font);
        styleDate.setAlignment(HorizontalAlignment.CENTER);

        //Se rellenan las celdas con los datos de FichaServicio
        for (SPV item : spv) {
            row = sheet.createRow(rowNum);
            cell = row.createCell(0);
            cell.setCellValue(item.getServicio());
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(style);

            cell = row.createCell(1);
            cell.setCellValue(item.getFecha());
            cell.setCellStyle(styleDate);

            cell = row.createCell(2);
            cell.setCellValue(item.getMarca());
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(style);

            cell = row.createCell(3);
            cell.setCellValue(item.getModelo());
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(style);

            cell = row.createCell(4);
            cell.setCellValue(item.getAño());
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(style);

            cell = row.createCell(5);
            cell.setCellValue(item.getPatente());
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(style);
            rowNum++;
        }

        //Se configura la respuesta del servidor
        response.setHeader("Content-Encoding", "UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        //response.setContentType("application/octet-stream");
    }
}
