package cl.ubb.TallerMecanico.reports;

import cl.ubb.TallerMecanico.domain.SPV;
import cl.ubb.TallerMecanico.domain.SPV;
import cl.ubb.TallerMecanico.reports.AbstractITextPdfView;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ServiceRecordPdfBuilder extends AbstractITextPdfView {
    final static DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
    final static String fileName = "Reporte_SPV_" + dateFormat.format(new Date()).toString() + ".pdf";

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Se obtiene el modelo que contiene las listas
        Map<String, Object> listsModel = (Map<String, Object>) model.get("model");

        //Se obtienen las listas contenidas en el modelo
        List<SPV> spv = (List<SPV>) listsModel.get("spv");

        //Calculo del tamaño de la tabla
        int tableSize = spv.size();

        //Se añade un título
        document.add(new Paragraph("Resumen de servicios por vehículo:"));

        //Configuración de la tabla
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[] {4.0f, 2.0f, 2.0f, 2.0f, 1.0f, 1.5f});
        table.setSpacingBefore(10);

        //Declaración de la fuente
        Font headerFont = FontFactory.getFont(FontFactory.HELVETICA);
        headerFont.setColor(BaseColor.BLACK);
        headerFont.setSize(12);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.BLACK);
        font.setSize(11);

        //Declaración de las cabeceras de la tabla
        PdfPCell headerCell = new PdfPCell();
        headerCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        headerCell.setPadding(5);
        headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);


        // write table header
        headerCell.setPhrase(new Phrase("Servicio", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Fecha", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Marca", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Modelo", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Año", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Patente", headerFont));
        table.addCell(headerCell);

        //Se añaden los datos de las listas
        for (SPV item : spv) {
            cell.setPhrase(new Phrase(item.getServicio(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(item.getFecha().toString(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(item.getMarca(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(item.getModelo(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(item.getAño(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(item.getPatente(), font));
            table.addCell(cell);
        }

        document.add(table);

        //Se configura la respuesta del servidor
        response.setHeader("Content-Encoding", "UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        //response.setContentType("application/octet-stream");
    }
}
