package cl.ubb.TallerMecanico.reports;

import cl.ubb.TallerMecanico.domain.Herramienta;
import cl.ubb.TallerMecanico.domain.Maquinaria;
import cl.ubb.TallerMecanico.domain.Repuesto;
import cl.ubb.TallerMecanico.reports.AbstractITextPdfView;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class AdminPdfBuilder extends AbstractITextPdfView {
    final static DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
    final static String fileName = "Reporte_Admin_" + dateFormat.format(new Date()).toString() + ".pdf";

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Se obtiene el modelo que contiene las listas
        Map<String, Object> listsModel = (Map<String, Object>) model.get("model");

        //Se obtienen las listas contenidas en el modelo
        List<Herramienta> herramientas = (List<Herramienta>) listsModel.get("herramientas");
        List<Maquinaria> maquinarias = (List<Maquinaria>) listsModel.get("maquinarias");
        List<Repuesto> repuestos = (List<Repuesto>) listsModel.get("repuestos");

        //Calculo del tamaño de la tabla
        int tableSize = herramientas.size() + maquinarias.size() + repuestos.size() + 1;

        //Se añade un título
        document.add(new Paragraph("Resumen de inventarios:"));

        //Configuración de la tabla
        PdfPTable table = new PdfPTable(5);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[] {1.2f, 4.0f, 2.0f, 2.0f, 1.8f});
        table.setSpacingBefore(10);

        //Declaración de la fuente
        Font headerFont = FontFactory.getFont(FontFactory.HELVETICA);
        headerFont.setColor(BaseColor.BLACK);
        headerFont.setSize(12);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.BLACK);
        font.setSize(11);

        //Declaración de las cabeceras de la tabla
        PdfPCell headerCell = new PdfPCell();
        headerCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        headerCell.setPadding(5);
        headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);


        // write table header
        headerCell.setPhrase(new Phrase("Código", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Nombre", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Tipo", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Estado", headerFont));
        table.addCell(headerCell);

        headerCell.setPhrase(new Phrase("Precio", headerFont));
        table.addCell(headerCell);

        //Se añaden los datos de las listas
        for (Herramienta herramienta : herramientas) {
            cell.setPhrase(new Phrase(herramienta.getCodigo(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(herramienta.getNombre(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Herramienta", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("-", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("-", font));
            table.addCell(cell);
        }

        for (Maquinaria maquinaria : maquinarias) {
            cell.setPhrase(new Phrase(maquinaria.getCodigo(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(maquinaria.getNombre(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Maquinaria", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(maquinaria.getEstado(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("-", font));
            table.addCell(cell);
        }

        for (Repuesto repuesto : repuestos) {
            cell.setPhrase(new Phrase(repuesto.getCodigo(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase(repuesto.getNombre(), font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("Repuesto", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("-", font));
            table.addCell(cell);
            cell.setPhrase(new Phrase("$" + repuesto.getPrecio(), font));
            table.addCell(cell);
        }

        document.add(table);

        //Se configura la respuesta del servidor
        response.setHeader("Content-Encoding", "UTF-8");
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        //response.setContentType("application/octet-stream");
    }
}
