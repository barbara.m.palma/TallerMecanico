package cl.ubb.TallerMecanico.Exceptions;

public class ServicioNoEncontradoException extends RuntimeException {
    public ServicioNoEncontradoException(Integer id) {
        System.out.println("No se ha encotnrado el servicio con id " + id);
    }
}
