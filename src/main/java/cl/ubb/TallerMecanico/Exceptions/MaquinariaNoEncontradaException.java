package cl.ubb.TallerMecanico.Exceptions;

public class MaquinariaNoEncontradaException extends RuntimeException {
    public MaquinariaNoEncontradaException(Integer id) {
        System.out.println("No se ha encotnrado la maquinaria con id " + id);
    }
}
