package cl.ubb.TallerMecanico.Exceptions;

public class HerramientaNoEncontradaException extends RuntimeException {

    public HerramientaNoEncontradaException(Integer id) {
        System.out.println("No se ha encotnrado la herramienta con id " + id);
    }
}
