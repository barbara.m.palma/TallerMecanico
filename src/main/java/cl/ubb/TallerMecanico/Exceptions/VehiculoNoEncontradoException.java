package cl.ubb.TallerMecanico.Exceptions;

public class VehiculoNoEncontradoException extends RuntimeException  {
    public VehiculoNoEncontradoException(Integer id) {
        System.out.println("No se ha encotnrado el vehiculo con id " + id);
    }
}
