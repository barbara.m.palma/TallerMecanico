package cl.ubb.TallerMecanico.Exceptions;

public class ClienteNoEncontradoException extends RuntimeException {

    //Excepcion lanzada cuando no se encuentra un Cliente
    //No se encuentra un cliente cuando se busca por su id
    public ClienteNoEncontradoException(Integer id) {
        System.out.println("No existe un cliente con el id " + id);
    }

    //No se encuentra un cliente cuando se busca por su nombre
    public ClienteNoEncontradoException(String nombre) {
        System.out.println("No existe un cliente con el nombre " + nombre);
    }
}
