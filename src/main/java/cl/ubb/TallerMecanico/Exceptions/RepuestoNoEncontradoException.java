package cl.ubb.TallerMecanico.Exceptions;

public class RepuestoNoEncontradoException extends RuntimeException {
    public RepuestoNoEncontradoException(Integer id) {
        System.out.println("No se ha encotnrado el repuesto con id " + id);
    }
}
