package cl.ubb.TallerMecanico.service;

import cl.ubb.TallerMecanico.domain.Ficha;
import cl.ubb.TallerMecanico.domain.FichaServicio;

import java.util.List;

public interface IFichaService {

    Ficha crear(Ficha ficha);

    List<FichaServicio> buscarPorVehiculo(int idVehiculo);

    List<Ficha> buscarTodas();

    Ficha buscarPorId(int id);

}
