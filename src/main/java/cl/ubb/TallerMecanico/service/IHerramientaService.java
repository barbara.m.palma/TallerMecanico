package cl.ubb.TallerMecanico.service;


import cl.ubb.TallerMecanico.domain.Herramienta;
import cl.ubb.TallerMecanico.domain.Herramienta2;

import java.util.List;

public interface IHerramientaService {

    List<Herramienta> buscarVisibles();

    List<Herramienta2> buscarTodos();

    Herramienta buscarPorId(Integer id);

    Herramienta eliminar(Integer id);

    Herramienta actualizar(Herramienta herramienta);

    Herramienta crear(Herramienta herramienta);
}

