package cl.ubb.TallerMecanico.service;

import cl.ubb.TallerMecanico.domain.FichaRUT;

import java.util.List;

public interface IFichaRutService {

    List<FichaRUT> buscarTodas();
}
