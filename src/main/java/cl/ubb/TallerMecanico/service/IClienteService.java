package cl.ubb.TallerMecanico.service;

import cl.ubb.TallerMecanico.domain.Cliente;

import java.util.List;

public interface IClienteService {

    //Interfaz de Service que muestra los métodos a implementar
    List<Cliente> buscarTodos();

    List<Cliente> buscarVisibles();

    Cliente buscarPorId(Integer id);

    Cliente buscarPorNombre(String name);

    Cliente crear(Cliente cliente);

    Cliente actualizar(Cliente cliente);

    Cliente borrarPorId(Integer id);

    Cliente borrar(Cliente cliente);



}
