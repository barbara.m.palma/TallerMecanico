package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.Exceptions.ServicioNoEncontradoException;
import cl.ubb.TallerMecanico.dao.IServicioDAO;
import cl.ubb.TallerMecanico.domain.Servicio;
import cl.ubb.TallerMecanico.service.IServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class ServicioService implements IServicioService {
    @Autowired
    private IServicioDAO servicioDAO;

    @Override
    public List<Servicio> buscarVisibles() {
        return servicioDAO.buscarVisibles();
    }

    @Override
    public Servicio buscarPorId(Integer id) {
        Servicio servicio = servicioDAO.buscarPorId(id);
        if (servicio==null){
            throw new ServicioNoEncontradoException(id);
        }
        return servicio;
    }

    @Override
    public Servicio actualizar(Servicio servicio) {
        Servicio s = servicioDAO.buscarPorId(servicio.getIdServicio());
        if (s==null){
            throw new ServicioNoEncontradoException(servicio.getIdServicio());
        }
        servicioDAO.actualizar(servicio);
        return servicio;
    }

    @Override
    public Servicio eliminar(Integer id) {
        Servicio servicio = servicioDAO.buscarPorId(id);
        if (servicio==null){
            throw new ServicioNoEncontradoException(id);
        }
        servicioDAO.eliminar(id);
        return servicio;
    }

    @Override
    public Servicio Ingresar(Servicio servicio) {
        servicioDAO.Ingresar(servicio);
        return servicio;
    }
}
