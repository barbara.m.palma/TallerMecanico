package cl.ubb.TallerMecanico.service.implementation;


import cl.ubb.TallerMecanico.dao.IAdministrativoDAO;
import cl.ubb.TallerMecanico.domain.Administrativo;
import cl.ubb.TallerMecanico.service.IAdministrativoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AdministrativoService implements IAdministrativoService {

    @Autowired
    private IAdministrativoDAO administrativoDAO;

    @Override
    public List<Administrativo> buscarTodos() {
        return administrativoDAO.buscarTodos();
    }

    @Override
    public Administrativo buscarPorId(int id) {
        return administrativoDAO.buscarPorId(id);
    }

}
