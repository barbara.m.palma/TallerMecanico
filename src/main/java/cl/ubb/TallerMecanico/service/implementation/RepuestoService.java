package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.Exceptions.RepuestoNoEncontradoException;
import cl.ubb.TallerMecanico.dao.IRepuestoDAO;
import cl.ubb.TallerMecanico.domain.Repuesto;
import cl.ubb.TallerMecanico.domain.Repuesto2;
import cl.ubb.TallerMecanico.service.IRepuestoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RepuestoService implements IRepuestoService {

    @Autowired
    private IRepuestoDAO repuestoDAO;

    @Override
    public List<Repuesto> buscarVisibles() {
        return repuestoDAO.buscarVisibles();
    }

    @Override
    public List<Repuesto2> buscarTodos() {
        return repuestoDAO.buscarTodos();
    }

    @Override
    public Repuesto buscarPorId(Integer id) {
        Repuesto repuesto = repuestoDAO.buscarPorId(id);
        if (repuesto == null){
            throw new RepuestoNoEncontradoException(id);
        }
        return repuesto;
    }

    @Override
    public Repuesto eliminar(Integer id) {
        Repuesto repuesto = repuestoDAO.buscarPorId(id);
        if (repuesto == null){
            throw new RepuestoNoEncontradoException(id);
        }
        repuestoDAO.eliminar(id);
        return repuesto;
    }

    @Override
    public Repuesto actualizar(Repuesto repuesto) {
        Repuesto m = repuestoDAO.buscarPorId(repuesto.getIdRepuesto());
        if (m == null){
            throw new RepuestoNoEncontradoException(repuesto.getIdRepuesto());
        }
        repuestoDAO.actualizar(repuesto);
        return repuesto;
    }

    @Override
    public Repuesto crear(Repuesto repuesto) {
        repuestoDAO.crear(repuesto);
        return repuesto;
    }
}
