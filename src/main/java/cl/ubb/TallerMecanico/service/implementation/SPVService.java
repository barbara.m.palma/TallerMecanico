package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.dao.ISPVDAO;
import cl.ubb.TallerMecanico.dao.implementation.SPVDAO;
import cl.ubb.TallerMecanico.domain.SPV;
import cl.ubb.TallerMecanico.service.ISPVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class SPVService implements ISPVService {

    @Autowired
    ISPVDAO SPVDao;

    @Override
    public List<SPV> buscarTodos() {
        return SPVDao.buscarTodos();
    }
}
