package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.dao.IFichaDAO;
import cl.ubb.TallerMecanico.domain.Ficha;
import cl.ubb.TallerMecanico.domain.FichaServicio;
import cl.ubb.TallerMecanico.service.IFichaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class FichaService implements IFichaService {

    @Autowired
    private IFichaDAO FichaDAO;

    @Override
    public Ficha crear(Ficha ficha) {
        return FichaDAO.crear(ficha);
    }

    @Override
    public List<FichaServicio> buscarPorVehiculo(int idVehiculo) {
        return FichaDAO.buscarPorVehiculo(idVehiculo);
    }

    @Override
    public List<Ficha> buscarTodas() {
        return FichaDAO.buscarTodas();
    }

    @Override
    public Ficha buscarPorId(int id) {
        return FichaDAO.buscarPorId(id);
    }


}
