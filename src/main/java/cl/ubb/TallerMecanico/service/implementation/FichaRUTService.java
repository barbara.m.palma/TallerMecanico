package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.dao.IFichaRUTDAO;
import cl.ubb.TallerMecanico.domain.FichaRUT;
import cl.ubb.TallerMecanico.service.IFichaRutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class FichaRUTService implements IFichaRutService {

    @Autowired
    private IFichaRUTDAO fichaRUTDAO;

    @Override
    public List<FichaRUT> buscarTodas() {
        return fichaRUTDAO.buscarTodas();
    }

}
