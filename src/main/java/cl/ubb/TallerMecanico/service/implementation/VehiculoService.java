package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.Exceptions.HerramientaNoEncontradaException;
import cl.ubb.TallerMecanico.Exceptions.VehiculoNoEncontradoException;
import cl.ubb.TallerMecanico.dao.IVehiculoDAO;
import cl.ubb.TallerMecanico.domain.Vehiculo;
import cl.ubb.TallerMecanico.service.IVehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;


@Service
@Transactional
public class VehiculoService implements IVehiculoService {

    @Autowired
    private IVehiculoDAO vehiculoDAO;

    @Override
    public List<Vehiculo> buscarVisibles() {

        return vehiculoDAO.buscarVisibles();
    }

    @Override
    public Vehiculo buscarPorId(Integer id) {
        Vehiculo vehiculo = vehiculoDAO.buscarPorId(id);
        if(vehiculo==null){
            throw new VehiculoNoEncontradoException(id);
        }
        return vehiculo;
    }

    @Override
    public Vehiculo eliminar(Integer id) {
        Vehiculo vehiculo = vehiculoDAO.buscarPorId(id);
        if(vehiculo==null){
            throw new VehiculoNoEncontradoException(id);
        }
        vehiculoDAO.eliminar(id);
        return vehiculo;
    }

    @Override
    public Vehiculo actualizar(Vehiculo vehiculo) {
        Vehiculo h = vehiculoDAO.buscarPorId(vehiculo.getIdVehiculo());
        if(h==null){
            throw new VehiculoNoEncontradoException(vehiculo.getIdVehiculo());
        }
        vehiculoDAO.actualizar(vehiculo);
        return vehiculo;
    }

    @Override
    public Vehiculo crear(Vehiculo vehiculo) {
        vehiculoDAO.crear(vehiculo);
        return vehiculo;
    }
}
