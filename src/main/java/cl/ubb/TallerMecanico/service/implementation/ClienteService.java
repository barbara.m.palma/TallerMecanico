package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.Exceptions.ClienteNoEncontradoException;
import cl.ubb.TallerMecanico.dao.IClienteDAO;
import cl.ubb.TallerMecanico.domain.Cliente;
import cl.ubb.TallerMecanico.service.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//Clase que implementa los métodos planteados en la interfaz

@Service
@Transactional
public class ClienteService implements IClienteService {

    @Autowired
    private IClienteDAO ClienteDAO;


    //Métodos que llaman al DAO para realizar consultas a la base de datos
    @Override
    public List<Cliente> buscarTodos() {
        return ClienteDAO.buscarTodos();
    }

    @Override
    public List<Cliente> buscarVisibles() {
        return ClienteDAO.buscarVisibles();
    }

    @Override
    public Cliente buscarPorId(Integer id) {
        Cliente toReturn = ClienteDAO.buscarPorId(id);
        if (toReturn == null) {
            throw new ClienteNoEncontradoException(id);
        }
        return toReturn;
    }

    @Override
    public Cliente buscarPorNombre(String nombre) {
        Cliente toReturn = ClienteDAO.buscarPorNombre(nombre);
        if (toReturn == null) {
            throw new ClienteNoEncontradoException(nombre);
        }
        return toReturn;
    }

    @Override
    public Cliente crear(Cliente cliente) {
        return ClienteDAO.crear(cliente);
    }

    @Override
    public Cliente actualizar(Cliente cliente) {
        Cliente toReturn = ClienteDAO.buscarPorId(cliente.getIdCliente());
        if (toReturn == null) {
            throw new ClienteNoEncontradoException(cliente.getIdCliente());
        }
        ClienteDAO.actualizar(cliente);
        return cliente;
    }

    @Override
    public Cliente borrarPorId(Integer id) {
        Cliente toReturn = ClienteDAO.borrarPorId(id);
        if (toReturn == null) {
            throw new ClienteNoEncontradoException(id);
        }
        return toReturn;
    }

    @Override
    public Cliente borrar(Cliente cliente) {
        Cliente toReturn = ClienteDAO.borrar(cliente);
        if (toReturn == null) {
            throw new ClienteNoEncontradoException(cliente.getIdCliente());
        }
        return toReturn;
    }
}
