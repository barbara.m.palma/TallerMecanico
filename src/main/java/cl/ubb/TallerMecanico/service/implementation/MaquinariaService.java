package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.Exceptions.MaquinariaNoEncontradaException;
import cl.ubb.TallerMecanico.dao.IMaquinariaDAO;
import cl.ubb.TallerMecanico.domain.Maquinaria;
import cl.ubb.TallerMecanico.domain.Maquinaria2;
import cl.ubb.TallerMecanico.service.IMaquinariaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MaquinariaService implements IMaquinariaService{

    @Autowired
    private IMaquinariaDAO maquinariaDAO;

    @Override
    public List<Maquinaria> buscarVisibles() {

        return maquinariaDAO.buscarVisibles();
    }

    @Override
    public List<Maquinaria2> buscarTodos() {

        return maquinariaDAO.buscarTodos();
    }

    @Override
    public Maquinaria buscarPorId(Integer id) {
        Maquinaria maquinaria = maquinariaDAO.buscarPorId(id);
        if (maquinaria == null){
            throw new MaquinariaNoEncontradaException(id);
        }
        return maquinaria;
    }

    @Override
    public Maquinaria eliminar(Integer id) {
        Maquinaria maquinaria = maquinariaDAO.buscarPorId(id);
        if (maquinaria == null){
            throw new MaquinariaNoEncontradaException(id);
        }
        maquinariaDAO.eliminar(id);
        return maquinaria;
    }

    @Override
    public Maquinaria actualizar(Maquinaria maquinaria) {
        Maquinaria m = maquinariaDAO.buscarPorId(maquinaria.getIdMaquinaria());
        if (m == null){
            throw new MaquinariaNoEncontradaException(maquinaria.getIdMaquinaria());
        }
        maquinariaDAO.actualizar(maquinaria);
        return maquinaria;
    }

    @Override
    public Maquinaria crear(Maquinaria maquinaria) {
        maquinariaDAO.crear(maquinaria);
        return maquinaria;
    }
}
