package cl.ubb.TallerMecanico.service.implementation;

import cl.ubb.TallerMecanico.Exceptions.HerramientaNoEncontradaException;
import cl.ubb.TallerMecanico.dao.IHerramientaDAO;
import cl.ubb.TallerMecanico.domain.Herramienta;
import cl.ubb.TallerMecanico.domain.Herramienta2;
import cl.ubb.TallerMecanico.service.IHerramientaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HerramientaService implements IHerramientaService {

    @Autowired
    private IHerramientaDAO herramientaDAO;

    @Override
    public List<Herramienta> buscarVisibles() {

        return herramientaDAO.buscarVisibles();
    }

    @Override
    public List<Herramienta2> buscarTodos() {

        return herramientaDAO.buscarTodos();
    }

    @Override
    public Herramienta buscarPorId(Integer id) {
        Herramienta herramienta = herramientaDAO.buscarPorId(id);
        if(herramienta==null){
            throw new HerramientaNoEncontradaException(id);
        }
        return herramienta;
    }

    @Override
    public Herramienta eliminar(Integer id) {
        Herramienta herramienta = herramientaDAO.buscarPorId(id);
        if(herramienta==null){
            throw new HerramientaNoEncontradaException(id);
        }
        herramientaDAO.eliminar(id);
        return herramienta;
    }

    @Override
    public Herramienta actualizar(Herramienta herramienta) {
        Herramienta h = herramientaDAO.buscarPorId(herramienta.getIdHerramienta());
        if(h==null){
            throw new HerramientaNoEncontradaException(herramienta.getIdHerramienta());
        }
        herramientaDAO.actualizar(herramienta);
        return herramienta;
    }

    @Override
    public Herramienta crear(Herramienta herramienta) {
        herramientaDAO.crear(herramienta);
        return herramienta;
    }
}
