package cl.ubb.TallerMecanico.service;

import cl.ubb.TallerMecanico.dao.implementation.AdministrativoDAO;
import cl.ubb.TallerMecanico.domain.Administrativo;

import java.util.List;

public interface IAdministrativoService {

    List<Administrativo> buscarTodos();

    Administrativo buscarPorId(int id);

}
