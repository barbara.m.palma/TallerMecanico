package cl.ubb.TallerMecanico.service;

import cl.ubb.TallerMecanico.domain.Servicio;

import java.util.List;

public interface IServicioService {

    List<Servicio> buscarVisibles();

    Servicio buscarPorId(Integer id);

    Servicio actualizar(Servicio servicio);

    Servicio eliminar(Integer id);

    Servicio Ingresar(Servicio servicio);
}
