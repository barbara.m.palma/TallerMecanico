package cl.ubb.TallerMecanico.service;


import cl.ubb.TallerMecanico.domain.Maquinaria;
import cl.ubb.TallerMecanico.domain.Maquinaria2;

import java.util.List;

public interface IMaquinariaService {

    List<Maquinaria> buscarVisibles();

    List<Maquinaria2> buscarTodos();

    Maquinaria buscarPorId(Integer id);

    Maquinaria eliminar(Integer id);

    Maquinaria actualizar(Maquinaria maquinaria);

    Maquinaria crear(Maquinaria maquinaria);
}

