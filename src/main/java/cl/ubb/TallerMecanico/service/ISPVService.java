package cl.ubb.TallerMecanico.service;

import cl.ubb.TallerMecanico.domain.SPV;

import java.util.List;

public interface ISPVService {

    List<SPV> buscarTodos();

}
