package cl.ubb.TallerMecanico.service;

import cl.ubb.TallerMecanico.domain.Vehiculo;

import java.util.List;

public interface IVehiculoService {

    List<Vehiculo> buscarVisibles();

    Vehiculo buscarPorId(Integer id);

    Vehiculo eliminar(Integer id);

    Vehiculo actualizar(Vehiculo vehiculo);

    Vehiculo crear(Vehiculo vehiculo);
}
