package cl.ubb.TallerMecanico.service;


import cl.ubb.TallerMecanico.domain.Repuesto;
import cl.ubb.TallerMecanico.domain.Repuesto2;

import java.util.List;

public interface IRepuestoService {

    List<Repuesto> buscarVisibles();

    List<Repuesto2> buscarTodos();

    Repuesto buscarPorId(Integer id);

    Repuesto eliminar(Integer id);

    Repuesto actualizar(Repuesto repuesto);

    Repuesto crear(Repuesto repuesto);
}

