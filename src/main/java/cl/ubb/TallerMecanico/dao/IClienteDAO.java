package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.Cliente;

import java.util.List;

public interface IClienteDAO {

    //Interfaz que define los métodos a implementar

    List<Cliente> buscarTodos();

    Cliente buscarPorId(Integer id);

    Cliente buscarPorNombre(String name);

    List<Cliente> buscarVisibles();

    Cliente crear(Cliente cliente);

    Cliente actualizar(Cliente cliente);

    Cliente borrarPorId(Integer id);

    Cliente borrar(Cliente cliente);

}
