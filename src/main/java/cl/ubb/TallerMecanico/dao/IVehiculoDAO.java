package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.Vehiculo;

import java.util.List;

public interface IVehiculoDAO {

    List<Vehiculo> buscarVisibles();

    Vehiculo buscarPorId(Integer id);

    Vehiculo actualizar(Vehiculo vehiculo);

    Vehiculo eliminar(Integer id);

    Vehiculo crear(Vehiculo vehiculo);
}
