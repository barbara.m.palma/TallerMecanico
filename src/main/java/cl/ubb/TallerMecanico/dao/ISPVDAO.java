package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.SPV;

import java.util.List;

public interface ISPVDAO {

    List<SPV> buscarTodos();

}
