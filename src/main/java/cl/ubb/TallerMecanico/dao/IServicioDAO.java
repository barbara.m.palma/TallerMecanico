package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.Servicio;

import java.util.List;

public interface IServicioDAO {
    List<Servicio> buscarVisibles();

    Servicio buscarPorId(Integer id);

    Servicio actualizar(Servicio servicio);

    Servicio eliminar(Integer id);

    Servicio Ingresar(Servicio servicio);
}
