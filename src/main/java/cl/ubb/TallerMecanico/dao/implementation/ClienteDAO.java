package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IClienteDAO;
import cl.ubb.TallerMecanico.domain.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.List;

//Clase que implementa los métodos planteados en la interfaz
@Repository
public class ClienteDAO implements IClienteDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    //RowMapper que toma los datos recogidos de la base de datos y los convierte en un objeto tipo cliente
    RowMapper<Cliente> rowMapper = (rs, rowNum) -> {
        int idCliente = rs.getInt("idcliente");
        String rut = rs.getString("rut");
        String nombre = rs.getString("nombre");
        String apellido = rs.getString("apellido");
        boolean visible = rs.getBoolean("visible");
        String telefono = rs.getString("telefono");
        String correo = rs.getString("correo");

        Cliente cliente = new Cliente();
        cliente.setIdCliente(idCliente);
        cliente.setRut(rut);
        cliente.setNombre(nombre);
        cliente.setApellido(apellido);
        cliente.setVisible(visible);
        cliente.setTelefono(telefono);
        cliente.setCorreo(correo);

        return cliente;
    };

    //Métodos que realizan consultas a la base de datos, haciendo uso de jdbcTemplate
    //Retornan un Cliente o una lista de Clientes según sea necesario
    @Override
    public List<Cliente> buscarTodos() {
        String sql = "SELECT * FROM cliente"; //Se crea un String con la consulta deseada en SQL
        List<Cliente> clientes = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return clientes;
    }

    @Override
    public Cliente buscarPorId(Integer id) {
        String condicion = "idcliente = " + Integer.toString(id);
        String sql = "SELECT * FROM cliente WHERE " + condicion;
        Cliente cliente = jdbcTemplate.queryForObject(sql, rowMapper); //En caso de que la consulta solo retorne 1 objeto, se usa el método queryForObject
        return cliente;
    }

    @Override
    public Cliente buscarPorNombre(String nombre) {
        String condicion = "upper(nombre) = '" + nombre.toUpperCase() + "'";
        String sql = "SELECT * FROM cliente WHERE " + condicion;
        Cliente cliente = jdbcTemplate.queryForObject(sql, rowMapper);
        return cliente;
    }

    @Override
    public List<Cliente> buscarVisibles() {
        String sql = "SELECT * FROM cliente WHERE visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Cliente> clientes = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return clientes;
    }
    @Override
    public Cliente crear(Cliente cliente) {
        String sql = "INSERT INTO cliente (rut, nombre ,apellido, visible, telefono, correo) VALUES (?,?,?,?,?,?)";

        KeyHolder keyHolder = new GeneratedKeyHolder(); //Se crea un objeto KeyHolder para generar la id de manera automática
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, cliente.getRut()); //Se setean los datos del objeto cliente a las columnas de la tabla
                    ps.setString(2, cliente.getNombre());
                    ps.setString(3, cliente.getApellido());
                    ps.setBoolean(4, cliente.getVisible());
                    ps.setString(5,cliente.getTelefono());
                    ps.setString(6,cliente.getCorreo());
                    return ps;
                },
                keyHolder);

        cliente.setIdCliente(keyHolder.getKey().intValue()); //Se genera el id
        return cliente;
    }

    @Override
    public Cliente actualizar(Cliente cliente) {
        String sql = "UPDATE cliente SET rut=?, nombre=?, apellido=?, visible=?, telefono=? , correo=? WHERE idcliente=?";
        jdbcTemplate.update(sql, cliente.getRut(), cliente.getNombre(), cliente.getApellido(),cliente.getVisible(),cliente.getTelefono(),cliente.getCorreo(), cliente.getIdCliente()
        ); //para actualizar se usa el método update de jdbcTemplate y se entrega la sentencia SQL y los parámetros a actualizar
        return cliente;
    }

    @Override
    public Cliente borrarPorId(Integer id) {
        String query = "SELECT * FROM cliente WHERE idcliente="+id;
        String sql = "DELETE FROM cliente WHERE idcliente=?";

        Cliente cliente = jdbcTemplate.queryForObject(query, rowMapper);
        jdbcTemplate.update(sql, id);
        return cliente;
    }

    @Override
    public Cliente borrar(Cliente cliente) {
        String sql = "DELETE FROM users WHERE idcliente=?";
        jdbcTemplate.update(sql, cliente.getIdCliente());
        return cliente;
    }


}
