package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IAdministrativoDAO;
import cl.ubb.TallerMecanico.domain.Administrativo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdministrativoDAO implements IAdministrativoDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    //RowMapper que toma los datos recogidos de la base de datos y los convierte en un objeto tipo administrativo
    RowMapper<Administrativo> rowMapper = (rs, rowNum) -> {
        int idAdministrativo = rs.getInt("idadministrativo");
        String rut = rs.getString("rut");
        String nombre = rs.getString("nombre");
        String apellido = rs.getString("apellido");
        boolean visible = rs.getBoolean("visible");


        Administrativo admin = new Administrativo();
        admin.setAdAdministrativo(idAdministrativo);
        admin.setRut(rut);
        admin.setNombre(nombre);
        admin.setApellido(apellido);
        admin.setVisible(visible);

        return admin;
    };

    @Override
    public List<Administrativo> buscarTodos() {
        String sql = "SELECT * FROM administrativo"; //Se crea un String con la consulta deseada en SQL
        List<Administrativo> admins = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return admins;
    }

    @Override
    public Administrativo buscarPorId(int id) {
        String condicion = " idadministrativo = " + Integer.toString(id);
        String sql = "SELECT * FROM administrativo where " + condicion; //Se crea un String con la consulta deseada en SQL
        Administrativo admin = jdbcTemplate.queryForObject(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return admin;
    }

}
