package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IFichaRUTDAO;
import cl.ubb.TallerMecanico.domain.FichaRUT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public class FichaRUTDAO implements IFichaRUTDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<FichaRUT> rowMapper = (rs, rowNum) -> {
        Integer idFicha = rs.getInt("idficha");
        String patente = rs.getString("patente");
        Date fecha = rs.getDate("fecha");
        String nombre = rs.getString("nombre");
        String detalle = rs.getString("detalle");
        String precio = rs.getString("precio");
        String rutCliente = rs.getString("rutcliente");
        String rutAdmin = rs.getString("rutadmin");

        FichaRUT fichaRut = new FichaRUT();
        fichaRut.setIdFicha(idFicha);
        fichaRut.setFecha(fecha);
        fichaRut.setNombre(nombre);
        fichaRut.setDetalle(detalle);
        fichaRut.setPrecio(precio);
        fichaRut.setRutCliente(rutCliente);
        fichaRut.setRutAdmin(rutAdmin);
        fichaRut.setPatente(patente);

        return fichaRut;
    };

    @Override
    public List<FichaRUT> buscarTodas() {
        String sql = "select f.idficha, v.patente, f.fecha, s.detalle as nombre, f.detalle, s.precio, c.rut as rutcliente, a.rut as rutadmin from ficha f join servicio s on f.idservicio = s.idservicio join cliente c on f.idcliente = c.idcliente join administrativo a on f.idadministrativo = a.idadministrativo JOIN vehiculo v on v.idvehiculo = f.idvehiculo"; //Se crea un String con la consulta deseada en SQL
        List<FichaRUT> fichas = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return fichas;
    }

}
