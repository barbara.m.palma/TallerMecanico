package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IMaquinariaDAO;
import cl.ubb.TallerMecanico.domain.Maquinaria;
import cl.ubb.TallerMecanico.domain.Maquinaria2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class MaquinariaDAO implements IMaquinariaDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<Maquinaria> rowMapper = (rs, rowNum) -> {
        int idMaquinaria = rs.getInt("idmaquinaria");
        String codigo = rs.getString("codigo");
        String nombre = rs.getString("nombre");
        String estado = rs.getString("estado");;
        Integer idAdministrativo = rs.getInt("idadministrativo");
        boolean visible = rs.getBoolean("visible");

        Maquinaria maquinaria = new Maquinaria();

        maquinaria.setIdMaquinaria(idMaquinaria);
        maquinaria.setCodigo(codigo);
        maquinaria.setNombre(nombre);
        maquinaria.setEstado(estado);
        maquinaria.setIdAdministrativo(idAdministrativo);
        maquinaria.setVisible(visible);
        return maquinaria;
    };

    RowMapper<Maquinaria2> rowMapper2 = (rs, rowNum) -> {
        int idMaquinaria = rs.getInt(1);
        String codigo = rs.getString(2);
        String nombre = rs.getString(3);
        String estado = rs.getString(4);;
        String idAdministrativo = rs.getString(5);
        boolean visible = rs.getBoolean(6);

        Maquinaria2 maquinaria = new Maquinaria2();

        maquinaria.setIdMaquinaria(idMaquinaria);
        maquinaria.setCodigo(codigo);
        maquinaria.setNombre(nombre);
        maquinaria.setEstado(estado);
        maquinaria.setIdAdministrativo(idAdministrativo);
        maquinaria.setVisible(visible);
        return maquinaria;
    };

    @Override
    public List<Maquinaria2> buscarTodos() {
        String sql = "SELECT m.idMaquinaria, m.codigo, m.nombre, m.estado ,a.nombre, m.visible FROM maquinaria m JOIN "+
                "administrativo a ON m.idadministrativo=a.idadministrativo WHERE m.visible=true";; //Se crea un String con la consulta deseada en SQL
        List<Maquinaria2> maquinarias = jdbcTemplate.query(sql, rowMapper2); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return maquinarias;
    }

    @Override
    public List<Maquinaria> buscarVisibles() {
        String sql = "SELECT * FROM maquinaria WHERE visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Maquinaria> maquinarias = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return maquinarias;
    }

    @Override
    public Maquinaria buscarPorId(Integer id) {
        String condicion = "idmaquinaria = " + Integer.toString(id);
        String sql = "SELECT * FROM maquinaria WHERE " + condicion;
        Maquinaria maquinaria = jdbcTemplate.queryForObject(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return maquinaria;
    }

    @Override
    public Maquinaria actualizar(Maquinaria maquinaria) {
        String sql = "UPDATE maquinaria SET codigo=?, nombre=?, estado=?, idadministrativo=?, visible=? WHERE idmaquinaria=?";
        jdbcTemplate.update(sql, maquinaria.getCodigo(), maquinaria.getNombre(), maquinaria.getEstado(),
                maquinaria.getIdAdministrativo(), maquinaria.isVisible(), maquinaria.getIdMaquinaria());
        //para actualizar se usa el método update de jdbcTemplate y se entrega la sentencia SQL y los parámetros a actualizar
        return maquinaria;
    }

    @Override
    public Maquinaria eliminar(Integer id) {
        String query = "SELECT * FROM maquinaria WHERE idmaquinaria="+id;
        String sql = "DELETE FROM maquinaria WHERE idmaquinaria=?";

        Maquinaria maquinaria = jdbcTemplate.queryForObject(query, rowMapper);
        jdbcTemplate.update(sql, id);
        return maquinaria;
    }

    @Override
    public Maquinaria crear(Maquinaria maquinaria) {
        String sql = "INSERT INTO maquinaria (codigo, nombre, estado, idadministrativo, visible) VALUES (?,?,?,?,?)";
        System.out.println(maquinaria.getCodigo()+" "+maquinaria.getNombre()+" " + maquinaria.getEstado() + " "+maquinaria.getIdAdministrativo()+ " "+maquinaria.isVisible());

        KeyHolder keyHolder = new GeneratedKeyHolder(); //Se crea un objeto KeyHolder para generar la id de manera automática
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, maquinaria.getCodigo()); //Se setean los datos del objeto maaquinaria a las columnas de la tabla
                    ps.setString(2, maquinaria.getNombre());
                    ps.setString(3, maquinaria.getEstado());
                    ps.setInt(4, maquinaria.getIdAdministrativo());
                    ps.setBoolean(5, maquinaria.isVisible());
                    return ps;
                },
                keyHolder);

        maquinaria.setIdMaquinaria(keyHolder.getKey().intValue()); //Se genera el id
        return maquinaria;
    }
}
