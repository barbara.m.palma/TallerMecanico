package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IFichaDAO;
import cl.ubb.TallerMecanico.domain.Ficha;
import cl.ubb.TallerMecanico.domain.FichaRUT;
import cl.ubb.TallerMecanico.domain.FichaServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Date;
import java.util.List;

@Repository
public class FichaDAO implements IFichaDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    //RowMapper que toma los datos recogidos de la base de datos y los convierte en un objeto tipo cliente
    RowMapper<Ficha> rowMapper = (rs, rowNum) -> {
        int idFicha = rs.getInt("idficha");
        Date fecha = rs.getDate("fecha");
        String detalle = rs.getString("detalle");
        int idVehiculo = rs.getInt("idvehiculo");
        int idCliente = rs.getInt("idcliente");
        int idServicio = rs.getInt("idservicio");
        int idAdministrativo = rs.getInt("idadministrativo");
        boolean visible = rs.getBoolean("visible");

        Ficha ficha = new Ficha();
        ficha.setIdFicha(idFicha);
        ficha.setFecha(fecha);
        ficha.setDetalle(detalle);
        ficha.setIdVehiculo(idVehiculo);
        ficha.setIdCliente(idCliente);
        ficha.setIdServicio(idServicio);
        ficha.setIdAdministrativo(idAdministrativo);
        ficha.setVisible(visible);

        return ficha;
    };

    RowMapper<FichaServicio> rowMapperFichaServicio = (rs, rowNum) -> {
        Date fecha = rs.getDate("fecha");
        String nombre = rs.getString("nombre");
        String detalle = rs.getString("detalle");
        String precio = rs.getString("precio");
        String rutCliente = rs.getString("rutcliente");
        String nombreCliente = rs.getString("nombrecliente");
        String apellidoCliente = rs.getString("apellidocliente");
        String telefonoCliente = rs.getString("telefono");
        String rutAdmin = rs.getString("rutadmin");
        String nombreAdmin = rs.getString("nombreadmin");
        String apellidoAdmin = rs.getString("apellidoadmin");

        FichaServicio fichaServicio = new FichaServicio();
        fichaServicio.setFecha(fecha);
        fichaServicio.setNombre(nombre);
        fichaServicio.setDetalle(detalle);
        fichaServicio.setPrecio(precio);
        fichaServicio.setRutCliente(rutCliente);
        fichaServicio.setNombreCliente(nombreCliente);
        fichaServicio.setApellidoCliente(apellidoCliente);
        fichaServicio.setTelefonoCliente(telefonoCliente);
        fichaServicio.setRutAdmin(rutAdmin);
        fichaServicio.setNombreAdmin(nombreAdmin);
        fichaServicio.setApellidoAdmin(apellidoAdmin);

        return fichaServicio;
    };


    @Override
    public Ficha crear(Ficha ficha) {
        String sql = "INSERT INTO ficha (fecha, detalle ,idvehiculo, idcliente, idservicio, idadministrativo, visible) VALUES (?,?,?,?,?,?,?)";

        KeyHolder keyHolder = new GeneratedKeyHolder(); //Se crea un objeto KeyHolder para generar la id de manera automática
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setDate(1, ficha.getFecha());
                    ps.setString(2, ficha.getDetalle());
                    ps.setInt(3, ficha.getIdVehiculo());
                    ps.setInt(4,ficha.getIdCliente());
                    ps.setInt(5, ficha.getIdServicio());
                    ps.setInt(6, ficha.getIdAdministrativo());
                    ps.setBoolean(7,true);
                    return ps;
                },
                keyHolder);

        ficha.setIdFicha(keyHolder.getKey().intValue()); //Se genera el id
        return ficha;
    }

    @Override
    public List<FichaServicio> buscarPorVehiculo(int idV) {
        String condicion = " idvehiculo = " + Integer.toString(idV);
        String sql = "select f.fecha, s.detalle as nombre, f.detalle, s.precio, c.rut as rutcliente, c.nombre as nombrecliente, c.apellido as apellidocliente, c.telefono, a.rut as rutadmin, a.nombre as nombreadmin, a.apellido as apellidoadmin from ficha f join servicio s on f.idservicio = s.idservicio join cliente c on f.idcliente = c.idcliente join administrativo a on f.idadministrativo = a.idadministrativo where" + condicion; //Se crea un String con la consulta deseada en SQL
        List<FichaServicio> fichas = jdbcTemplate.query(sql, rowMapperFichaServicio); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return fichas;
    }

    @Override
    public List<Ficha> buscarTodas() {
        String sql = "select * from ficha"; //Se crea un String con la consulta deseada en SQL
        List<Ficha> fichas = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return fichas;
    }

    @Override
    public Ficha buscarPorId(int id) {
        String condicion = " idficha = " + Integer.toString(id);
        String sql = "select * from ficha where " + condicion; //Se crea un String con la consulta deseada en SQL
        Ficha ficha = jdbcTemplate.queryForObject(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return ficha;
    }


}
