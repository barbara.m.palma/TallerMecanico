package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IRepuestoDAO;
import cl.ubb.TallerMecanico.domain.Repuesto;
import cl.ubb.TallerMecanico.domain.Repuesto2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class RepuestoDAO implements IRepuestoDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<Repuesto> rowMapper = (rs, rowNum) -> {
        int idRepuesto = rs.getInt("idrepuesto");
        String codigo = rs.getString("codigo");
        String nombre = rs.getString("nombre");
        String precio = rs.getString("precio");;
        Integer idAdministrativo = rs.getInt("idadministrativo");
        boolean visible = rs.getBoolean("visible");

        Repuesto repuesto = new Repuesto();

        repuesto.setIdRepuesto(idRepuesto);
        repuesto.setCodigo(codigo);
        repuesto.setNombre(nombre);
        repuesto.setPrecio(precio);
        repuesto.setIdAdministrativo(idAdministrativo);
        repuesto.setVisible(visible);
        return repuesto;
    };

    RowMapper<Repuesto2> rowMapper2 = (rs, rowNum) -> {
        int idRepuesto = rs.getInt(1);
        String codigo = rs.getString(2);
        String nombre = rs.getString(3);
        String precio = rs.getString(4);;
        String idAdministrativo = rs.getString(5);
        boolean visible = rs.getBoolean(6);

        Repuesto2 repuesto = new Repuesto2();

        repuesto.setIdRepuesto(idRepuesto);
        repuesto.setCodigo(codigo);
        repuesto.setNombre(nombre);
        repuesto.setPrecio(precio);
        repuesto.setIdAdministrativo(idAdministrativo);
        repuesto.setVisible(visible);
        return repuesto;
    };

    @Override
    public List<Repuesto> buscarVisibles() {
        String sql = "SELECT * FROM repuesto WHERE visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Repuesto> repuestos = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return repuestos;
    }

    @Override
    public List<Repuesto2> buscarTodos() {
        String sql = "SELECT r.idrepuesto, r.codigo, r.nombre, r.precio ,a.nombre, r.visible FROM repuesto r JOIN "+
                "administrativo a ON r.idadministrativo=a.idadministrativo WHERE r.visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Repuesto2> repuestos = jdbcTemplate.query(sql, rowMapper2); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return repuestos;
    }

    @Override
    public Repuesto buscarPorId(Integer id) {
        String condicion = "idrepuesto = " + Integer.toString(id);
        String sql = "SELECT * FROM repuesto WHERE " + condicion;
        Repuesto repuesto = jdbcTemplate.queryForObject(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return repuesto;
    }

    @Override
    public Repuesto actualizar(Repuesto repuesto) {
        String sql = "UPDATE repuesto SET codigo=?, nombre=?, precio=?, idadministrativo=?, visible=? WHERE idrepuesto=?";
        jdbcTemplate.update(sql, repuesto.getCodigo(), repuesto.getNombre(), repuesto.getPrecio(),
                repuesto.getIdAdministrativo(), repuesto.isVisible(), repuesto.getIdRepuesto());
        //para actualizar se usa el método update de jdbcTemplate y se entrega la sentencia SQL y los parámetros a actualizar
        return repuesto;
    }

    @Override
    public Repuesto eliminar(Integer id) {
        String query = "SELECT * FROM repuesto WHERE idrepuesto="+id;
        String sql = "DELETE FROM repuesto WHERE idrepuesto=?";

        Repuesto repuesto = jdbcTemplate.queryForObject(query, rowMapper);
        jdbcTemplate.update(sql, id);
        return repuesto;
    }

    @Override
    public Repuesto crear(Repuesto repuesto) {
        String sql = "INSERT INTO repuesto (codigo, nombre, precio, idadministrativo, visible) VALUES (?,?,?,?,?)";
        System.out.println(repuesto.getCodigo()+" "+repuesto.getNombre()+" " + repuesto.isVisible() + " "+repuesto.getIdAdministrativo()+ " "+repuesto.isVisible());

        KeyHolder keyHolder = new GeneratedKeyHolder(); //Se crea un objeto KeyHolder para generar la id de manera automática
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, repuesto.getCodigo()); //Se setean los datos del objeto repuesto a las columnas de la tabla
                    ps.setString(2, repuesto.getNombre());
                    ps.setString(3, repuesto.getPrecio());
                    ps.setInt(4, repuesto.getIdAdministrativo());
                    ps.setBoolean(5, repuesto.isVisible());
                    return ps;
                },
                keyHolder);

        repuesto.setIdRepuesto(keyHolder.getKey().intValue()); //Se genera el id
        return repuesto;
    }
}
