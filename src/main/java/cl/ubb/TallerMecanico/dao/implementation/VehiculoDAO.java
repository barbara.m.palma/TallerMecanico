package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IVehiculoDAO;
import cl.ubb.TallerMecanico.domain.Vehiculo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class VehiculoDAO implements IVehiculoDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<Vehiculo> rowMapper = (rs, rowNum) -> {
        int idVehiculo = rs.getInt("idVehiculo");
        String marca = rs.getString("marca");
        String modelo = rs.getString("modelo");
        String anio = rs.getString("año");
        String patente = rs.getString("patente");
        Integer idCliente = rs.getInt("idCliente");
        boolean visible = rs.getBoolean("visible");

        Vehiculo vehiculo = new Vehiculo();
        vehiculo.setIdVehiculo(idVehiculo);
        vehiculo.setMarca(marca);
        vehiculo.setModelo(modelo);
        vehiculo.setAnio(anio);
        vehiculo.setPatente(patente);
        vehiculo.setIdCliente(idCliente);
        vehiculo.setVisible(visible);
        return vehiculo;
    };

    @Override
    public List<Vehiculo> buscarVisibles() {
        String sql = "SELECT * FROM vehiculo WHERE visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Vehiculo> vehiculos = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return vehiculos;
    }

    @Override
    public Vehiculo buscarPorId(Integer id) {
        String condicion = "idvehiculo = " + Integer.toString(id);
        String sql = "SELECT * FROM vehiculo WHERE " + condicion;
        Vehiculo vehiculo = jdbcTemplate.queryForObject(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return vehiculo;
    }

    @Override
    public Vehiculo actualizar(Vehiculo vehiculo) {
        String sql = "UPDATE vehiculo SET idvehiculo=?, marca=?, modelo=?, anio=?,patente =?,idcliente=?,visible=? WHERE idvehiculo=?";
        jdbcTemplate.update(sql, vehiculo.getIdVehiculo(), vehiculo.getMarca(),vehiculo.getModelo(),vehiculo.getAnio(),vehiculo.getPatente(),vehiculo.getIdCliente(),vehiculo.isVisible()
        ); //para actualizar se usa el método update de jdbcTemplate y se entrega la sentencia SQL y los parámetros a actualizar
        return vehiculo;
    }

    @Override
    public Vehiculo eliminar(Integer id) {
        String query = "SELECT * FROM vehiculo WHERE idvehiculo="+id;
        String sql = "DELETE FROM vehiculo WHERE idvehiculo=?";

        Vehiculo vehiculo = jdbcTemplate.queryForObject(query, rowMapper);
        jdbcTemplate.update(sql, id);
        return vehiculo;
    }

    @Override
    public Vehiculo crear(Vehiculo vehiculo) {
        String sql = "INSERT INTO vehiculo ( marca, modelo, anio,patente ,idcliente,visible) VALUES (?,?,?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder(); //Se crea un objeto KeyHolder para generar la id de manera automática
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, vehiculo.getMarca());
                    ps.setString(2, vehiculo.getModelo());
                    ps.setString(3, vehiculo.getAnio());
                    ps.setString(4, vehiculo.getPatente());
                    ps.setInt(5, vehiculo.getIdCliente());
                    ps.setBoolean(6, vehiculo.isVisible());
                    return ps;
                },
                keyHolder);

        vehiculo.setIdVehiculo(keyHolder.getKey().intValue()); //Se genera el id
        return vehiculo;
    }
}
