package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IServicioDAO;
import cl.ubb.TallerMecanico.domain.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class ServicioDAO implements IServicioDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<Servicio> rowMapper = (rs, rowNum) -> {
        int idServicio = rs.getInt("idservicio");
        String precio = rs.getString("precio");
        String detalle = rs.getString("detalle");
        boolean visible = rs.getBoolean("visible");

        Servicio servicio = new Servicio();
        servicio.setIdServicio(idServicio);
        servicio.setPrecio(precio);
        servicio.setDetalle(detalle);
        servicio.setVisible(visible);
        return servicio;
    };

    @Override
    public List<Servicio> buscarVisibles() {
        String sql = "SELECT * FROM servicio WHERE visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Servicio> servicios = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return servicios;
    }

    @Override
    public Servicio buscarPorId(Integer id) {
        String condicion = "idservicio = " + Integer.toString(id);
        String sql = "SELECT * FROM servicio WHERE " + condicion;
        Servicio servicio = jdbcTemplate.queryForObject(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return servicio;
    }

    @Override
    public Servicio actualizar(Servicio servicio) {
        String sql = "UPDATE servicio SET precio=?, detalle=?, visible=? WHERE idservicio=?";
        jdbcTemplate.update(sql, servicio.getPrecio(), servicio.getDetalle(), servicio.isVisible()); //para actualizar se usa el método update de jdbcTemplate y se entrega la sentencia SQL y los parámetros a actualizar
        return servicio;
    }

    @Override
    public Servicio eliminar(Integer id) {
        String query = "SELECT * FROM servicio WHERE idservicio="+id;
        String sql = "DELETE FROM servicio WHERE idservicio=?";

        Servicio servicio = jdbcTemplate.queryForObject(query, rowMapper);
        jdbcTemplate.update(sql, id);
        return servicio;
    }

    @Override
    public Servicio Ingresar(Servicio servicio) {
        String sql = "INSERT INTO servicio (precio, detalle, visible) VALUES (?,?,?)";
        System.out.println(servicio.getPrecio()+" "+servicio.getDetalle()+" "+servicio.getIdServicio()+ " "+servicio.isVisible());

        KeyHolder keyHolder = new GeneratedKeyHolder(); //Se crea un objeto KeyHolder para generar la id de manera automática
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, servicio.getPrecio()); //Se setean los datos del objeto cliente a las columnas de la tabla
                    ps.setString(2, servicio.getDetalle());
                    ps.setBoolean(3, servicio.isVisible());
                    return ps;
                },
                keyHolder);

        servicio.setIdServicio(keyHolder.getKey().intValue()); //Se genera el id
        return servicio;
    }
}
