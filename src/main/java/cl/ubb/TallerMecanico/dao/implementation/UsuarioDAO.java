package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IUsuarioDAO;
import cl.ubb.TallerMecanico.domain.InfoUsuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioDAO implements IUsuarioDAO {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Override
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public InfoUsuario getUserInfo(String username) {
        String query = "SELECT u.username, u.password, r.rol FROM usuario u JOIN rol_usuario r ON u.username = r.username WHERE u.username=?;";
        InfoUsuario infoUsuario = (InfoUsuario)jdbcTemplate.queryForObject(query, new Object[]{username},
                new RowMapper<InfoUsuario>() {
                    public InfoUsuario mapRow(ResultSet rs, int rowNum) throws SQLException {
                        InfoUsuario user = new InfoUsuario();
                        user.setUsername(rs.getString("username"));
                        user.setPassword(rs.getString("password"));
                        user.setRole(rs.getString("rol"));
                        return user;
                    }
                });
        return infoUsuario;
    }
}
