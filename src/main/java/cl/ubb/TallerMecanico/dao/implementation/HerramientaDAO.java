package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.IHerramientaDAO;
import cl.ubb.TallerMecanico.domain.Herramienta;
import cl.ubb.TallerMecanico.domain.Herramienta2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Repository
public class HerramientaDAO implements IHerramientaDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<Herramienta> rowMapper = (rs, rowNum) -> {
        int idHerramienta = rs.getInt("idherramienta");
        String codigo = rs.getString("codigo");
        String nombre = rs.getString("nombre");
        Integer idAdministrativo = rs.getInt("idadministrativo");
        boolean visible = rs.getBoolean("visible");

        Herramienta herramienta = new Herramienta();
        herramienta.setIdHerramienta(idHerramienta);
        herramienta.setCodigo(codigo);
        herramienta.setNombre(nombre);
        herramienta.setIdAdministrativo(idAdministrativo);
        herramienta.setVisible(visible);
        return herramienta;
    };

    RowMapper<Herramienta2> rowMapper2 = (rs, rowNum) -> {
        int idHerramienta = rs.getInt(1);
        String codigo = rs.getString(2);
        String nombre = rs.getString(3);
        String idAdministrativo = rs.getString(4);
        boolean visible = rs.getBoolean(5);

        Herramienta2 herramienta = new Herramienta2();
        herramienta.setIdHerramienta(idHerramienta);
        herramienta.setCodigo(codigo);
        herramienta.setNombre(nombre);
        herramienta.setIdAdministrativo(idAdministrativo);
        herramienta.setVisible(visible);
        return herramienta;
    };

    @Override
    public List<Herramienta2> buscarTodos() {
        String sql = "SELECT h.idherramienta, h.codigo, h.nombre, a.nombre, h.visible FROM herramienta h JOIN "+
                "administrativo a ON h.idadministrativo=a.idadministrativo WHERE h.visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Herramienta2> herramientas = jdbcTemplate.query(sql, rowMapper2); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return herramientas;
    }

    @Override
    public List<Herramienta> buscarVisibles() {
        String sql = "SELECT * FROM herramienta WHERE visible=true"; //Se crea un String con la consulta deseada en SQL
        List<Herramienta> herramientas = jdbcTemplate.query(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return herramientas;
    }

    @Override
    public Herramienta buscarPorId(Integer id) {
        String condicion = "idherramienta = " + Integer.toString(id);
        String sql = "SELECT * FROM herramienta WHERE " + condicion;
        Herramienta herramienta = jdbcTemplate.queryForObject(sql, rowMapper); //Se consulta a la base de datos usand jdbcTemplate y su método query, pasándole el rowMapper
        return herramienta;
    }

    @Override
    public Herramienta actualizar(Herramienta herramienta) {
        String sql = "UPDATE herramienta SET codigo=?, nombre=?, idadministrativo=?, visible=? WHERE idherramienta=?";
        jdbcTemplate.update(sql, herramienta.getCodigo(), herramienta.getNombre(), herramienta.getIdAdministrativo(),
                herramienta.getVisible(), herramienta.getIdHerramienta()
        ); //para actualizar se usa el método update de jdbcTemplate y se entrega la sentencia SQL y los parámetros a actualizar
        return herramienta;
    }

    @Override
    public Herramienta eliminar(Integer id) {
        String query = "SELECT * FROM herramienta WHERE idherramienta="+id;
        String sql = "DELETE FROM herramienta WHERE idherramienta=?";

        Herramienta herramienta = jdbcTemplate.queryForObject(query, rowMapper);
        jdbcTemplate.update(sql, id);
        return herramienta;
    }

    @Override
    public Herramienta crear(Herramienta herramienta) {
        String sql = "INSERT INTO herramienta (codigo, nombre, idadministrativo, visible) VALUES (?,?,?,?)";
        System.out.println(herramienta.getCodigo()+" "+herramienta.getNombre()+" "+herramienta.getIdAdministrativo()+ " "+herramienta.getVisible());

        KeyHolder keyHolder = new GeneratedKeyHolder(); //Se crea un objeto KeyHolder para generar la id de manera automática
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, herramienta.getCodigo()); //Se setean los datos del objeto cliente a las columnas de la tabla
                    ps.setString(2, herramienta.getNombre());
                    ps.setInt(3, herramienta.getIdAdministrativo());
                    ps.setBoolean(4, herramienta.getVisible());
                    return ps;
                },
                keyHolder);

        herramienta.setIdHerramienta(keyHolder.getKey().intValue()); //Se genera el id
        return herramienta;
    }
}
