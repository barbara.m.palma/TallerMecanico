package cl.ubb.TallerMecanico.dao.implementation;

import cl.ubb.TallerMecanico.dao.ISPVDAO;
import cl.ubb.TallerMecanico.domain.SPV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public class SPVDAO implements ISPVDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    RowMapper<SPV> rowMapper = (rs, rowNum) -> {
        int idFicha  = rs.getInt(1);;
        int idServicio  = rs.getInt(2);;
        Date fecha = rs.getDate(3);
        String servicio = rs.getString(4);
        String marca = rs.getString(5);
        String modelo =rs.getString(6);
        String año = rs.getString(7);
        String patente = rs.getString(8);

        SPV spv = new SPV();

        spv.setIdFicha(idFicha);
        spv.setIdServicio(idServicio);
        spv.setFecha(fecha);
        spv.setServicio(servicio);
        spv.setMarca(marca);
        spv.setModelo(modelo);
        spv.setAño(año);
        spv.setPatente(patente);
        return spv;
    };

    @Override
    public List<SPV> buscarTodos() {
        String query = "SELECT r.idficha, r.idservicio, r.fecha, r.detalle, v.marca, v.modelo, v.año, v.patente FROM vehiculo v JOIN " +
                "(SELECT f.idficha, f.idservicio, f.fecha, s.detalle, f.idvehiculo FROM ficha f JOIN servicio s ON " +
                "(f.idservicio=s.idservicio)) AS r ON v.idvehiculo=r.idvehiculo";
        List<SPV> spv = jdbcTemplate.query(query, rowMapper);
        return spv;
    }
}
