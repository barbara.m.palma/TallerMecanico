package cl.ubb.TallerMecanico.dao;

import java.util.List;
import cl.ubb.TallerMecanico.domain.Administrativo;

public interface IAdministrativoDAO {

    List<Administrativo> buscarTodos();

    Administrativo buscarPorId(int id);
}
