package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.InfoUsuario;

import javax.sql.DataSource;

public interface IUsuarioDAO {

    public void setDataSource(DataSource dataSource);

    public InfoUsuario getUserInfo(String username);
}
