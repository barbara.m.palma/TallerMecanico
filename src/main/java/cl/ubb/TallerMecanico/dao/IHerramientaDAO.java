package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.Herramienta;
import cl.ubb.TallerMecanico.domain.Herramienta2;

import java.util.List;

public interface IHerramientaDAO {

    List<Herramienta> buscarVisibles();

    List<Herramienta2> buscarTodos();

    Herramienta buscarPorId(Integer id);

    Herramienta actualizar(Herramienta herramienta);

    Herramienta eliminar(Integer id);

    Herramienta crear(Herramienta herramienta);

}

