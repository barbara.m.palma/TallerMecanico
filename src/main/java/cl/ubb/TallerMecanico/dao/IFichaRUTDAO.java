package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.FichaRUT;

import java.util.List;

public interface IFichaRUTDAO {

    List<FichaRUT> buscarTodas();
}
