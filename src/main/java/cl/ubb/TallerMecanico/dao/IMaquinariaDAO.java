package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.Maquinaria;
import cl.ubb.TallerMecanico.domain.Maquinaria2;

import java.util.List;

public interface IMaquinariaDAO {

    List<Maquinaria> buscarVisibles();

    List<Maquinaria2> buscarTodos();

    Maquinaria buscarPorId(Integer id);

    Maquinaria actualizar(Maquinaria maquinaria);

    Maquinaria eliminar(Integer id);

    Maquinaria crear(Maquinaria maquinaria);

}

