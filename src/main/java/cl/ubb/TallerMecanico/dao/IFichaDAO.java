package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.Ficha;
import cl.ubb.TallerMecanico.domain.FichaRUT;
import cl.ubb.TallerMecanico.domain.FichaServicio;

import java.util.List;

public interface IFichaDAO {

    Ficha crear(Ficha ficha);

    List<FichaServicio> buscarPorVehiculo(int idVehiculo);

    List<Ficha> buscarTodas();

    Ficha buscarPorId(int id);

}
