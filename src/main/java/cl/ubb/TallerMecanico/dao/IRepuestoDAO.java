package cl.ubb.TallerMecanico.dao;

import cl.ubb.TallerMecanico.domain.Repuesto;
import cl.ubb.TallerMecanico.domain.Repuesto2;

import java.util.List;

public interface IRepuestoDAO {

    List<Repuesto> buscarVisibles();

    List<Repuesto2> buscarTodos();

    Repuesto buscarPorId(Integer id);

    Repuesto actualizar(Repuesto repuesto);

    Repuesto eliminar(Integer id);

    Repuesto crear(Repuesto repuesto);

}

