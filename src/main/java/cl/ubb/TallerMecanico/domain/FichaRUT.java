package cl.ubb.TallerMecanico.domain;

import java.sql.Date;

public class FichaRUT {

    private Integer idFicha;
    private Date fecha;
    private String patente;
    private String nombre;
    private String detalle;
    private String precio;
    private String rutCliente;
    private String rutAdmin;

    public Integer getIdFicha() {
        return idFicha;
    }

    public void setIdFicha(Integer idFicha) {
        this.idFicha = idFicha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public String getRutAdmin() {
        return rutAdmin;
    }

    public void setRutAdmin(String rutAdmin) {
        this.rutAdmin = rutAdmin;
    }
}
