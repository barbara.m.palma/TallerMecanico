package cl.ubb.TallerMecanico.domain;

public class Administrativo {

    //Clase con los atributos correspondientes a la tabla administrativos

    private Integer adAdministrativo;
    private String rut;
    private String nombre;
    private String apellido;
    private boolean visible;

    //Getters y Setters

    public Integer getAdAdministrativo() {
        return adAdministrativo;
    }

    public void setAdAdministrativo(Integer adAdministrativo) {
        this.adAdministrativo = adAdministrativo;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
