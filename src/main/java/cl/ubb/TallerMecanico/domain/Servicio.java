package cl.ubb.TallerMecanico.domain;

public class Servicio {

    //Clase con los atributos correspondientes a la tabla Servicio

    private Integer idServicio;
    private String precio;
    private String detalle;
    private boolean visible;

    //Getters y Setters

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
